FROM ruby:3.0.2-alpine

ENV RAILS_ENV='test'
ENV RAILS_ENV='test'

ENV BUILD_PACKAGES="build-base curl-dev git curl wget shared-mime-info"
ENV DEV_PACKAGES="postgresql-dev yaml-dev zlib-dev nodejs "
ENV RUBY_PACKAGES="tzdata"

ENV RAILS_ROOT /var/www/app_name


# install packages
RUN apk update \
    && apk upgrade \
    && apk add --update --no-cache $BUILD_PACKAGES $DEV_PACKAGES $RUBY_PACKAGES \
    && apk add --no-cache yarn --repository="http://dl-cdn.alpinelinux.org/alpine/edge/community" \
    && yarn -v

# Install bundler
RUN gem update --system
#RUN gem install mimemagic -v 0.4.3
RUN gem install rails -v 7.0.0
RUN gem install rake -v 13.0.6
RUN gem install bundler -v 2.2.33
RUN which rails
RUN which bundler
RUN which rake


